import { ImprintContent } from '@components/ImprintContent';

const ImprintPage = () => {
  return <ImprintContent />;
};

export default ImprintPage;