import { Box, VStack, Heading, Text, Image, Link, Center, Divider, HStack, Icon } from '@chakra-ui/react';
import { FaLinkedin, FaTwitter, FaGithub, FaLink } from 'react-icons/fa';
import { useConfetti } from '@hooks/useConfetti';

const gradientBg = 'linear(135deg, rgba(255, 255, 255, 0.85) 0%, rgba(247, 247, 247, 0.85) 50%, rgba(237, 237, 237, 0.85) 100%)';
const iconHoverStyle = {
  bgGradient: 'linear(to-l, #7928CA, #FF0080)',
  bgClip: 'text'
};

const Home = () => {
  useConfetti();

  return (
    <Box bgGradient={gradientBg} color='gray.800' minH='100vh' py='12' px={{ base: '4', lg: '8' }}>
      <Center w='full' h='full'>
        <VStack spacing='8' align='center' w='full' maxW='lg'>
          <Image
            borderRadius='full'
            boxSize='150px'
            src='https://media.licdn.com/dms/image/D4E03AQEEIDe7V9k_9w/profile-displayphoto-shrink_800_800/0/1687302199230?e=1711584000&v=beta&t=EuY1SQpu8yQ9xN39KyySnHoApPjwWTrTxEQNeR5qnlk'
            alt='Marko Kraemer'
          />
          <Heading as='h1' size='2xl' color='gray.900' textAlign={{ base: 'center', lg: 'left' }}>
            Marko Kraemer
          </Heading>
          <Text fontSize='lg' fontWeight='semibold' color='gray.600' textAlign={{ base: 'center', lg: 'left' }}>
            CEO & Co-Founder at SoftGen.ai
          </Text>
          <Link href='https://www.softgen.ai' isExternal _focus={{ boxShadow: 'none' }} _hover={{ textDecoration: 'none' }}>
            <Text fontSize='lg' fontWeight='bold' bgGradient='linear(to-l, #7928CA, #FF0080)' bgClip='text' textAlign={{ base: 'center', lg: 'left' }}>
              Building the autonomous AI developer @SoftGen.ai
            </Text>
          </Link>
          <Divider my='8' />
          <VStack spacing='4' align='stretch'>
            <Text textAlign={{ base: 'center', lg: 'left' }} color='gray.600'>
              Hey, I'm Marko! I am 18 years old and I have been running software projects and companies since I was 13. I have scaled up a software development agency to 1+ million euros in revenue, and over the last few years, most of my time has been spent planning and managing custom software development projects. 
            </Text>
            <Text textAlign={{ base: 'center', lg: 'left' }} color='gray.600' bgGradient='linear(to-l, #7928CA, #FF0080)' bgClip='text'>
When I first used Generative AI to reduce task planning from 8 hours to 1.5, I knew a big change was coming to this industry. This led to the creation of SoftGen.AI, a complete natural language development environment with the end goal of creating the autonomous AI developer.
            </Text>
            <Text textAlign={{ base: 'center', lg: 'left' }} color='gray.600'>
              The AI revolution is reshaping economies. In a decade, it'll be unrecognizable.  Existing processes need to be rethought based on first-principle thinking in all industries; we will be part of that change.
            </Text>
            <Text textAlign={{ base: 'center', lg: 'left' }} color='gray.600' bgGradient='linear(to-l, #7928CA, #FF0080)' bgClip='text'>
              Autonomous developer AIs are inevitable. The tech is here, and it's time to execute. Its time for the future.
            </Text>
          </VStack>
          <Divider my='8' />
          <HStack spacing='5' justify={{ base: 'center', lg: 'left' }}>
            <Link href='https://www.linkedin.com/in/markokraemer/' isExternal _focus={{ boxShadow: 'none' }} _hover={{ bgGradient: 'linear(to-l, #7928CA, #FF0080)', bgClip: 'text', textDecoration: 'none' }}>
              <Icon as={FaLinkedin} w={6} h={6} color='gray.500'  />
            </Link>
            <Link href='https://twitter.com/markokraemer' isExternal _focus={{ boxShadow: 'none' }} _hover={{ bgGradient: 'linear(to-l, #7928CA, #FF0080)', bgClip: 'text', textDecoration: 'none' }}>
              <Icon as={FaTwitter} w={6} h={6} color='gray.500' />
            </Link>
            <Link href='https://github.com/markokraemer' isExternal _focus={{ boxShadow: 'none' }} _hover={{ bgGradient: 'linear(to-l, #7928CA, #FF0080)', bgClip: 'text', textDecoration: 'none' }}>
              <Icon as={FaGithub} w={6} h={6} color='gray.500' />
            </Link>
            <Link href='https://www.softgen.ai' isExternal _focus={{ boxShadow: 'none' }} _hover={{ bgGradient: 'linear(to-l, #7928CA, #FF0080)', bgClip: 'text', textDecoration: 'none' }}>
              <Icon as={FaLink} w={6} h={6} color='gray.500'  />
            </Link>
          </HStack>
          <Link href='/imprint' _focus={{ boxShadow: 'none' }} _hover={{ textDecoration: 'underline' }}>
            <Text color='gray.600'>Imprint</Text>
          </Link>
        </VStack>
      </Center>
    </Box>
  );
};

export default Home;
