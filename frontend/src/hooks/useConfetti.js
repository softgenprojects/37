import { useEffect } from 'react';
import { startConfetti } from '@utilities/confettiUtils';

export const useConfetti = () => {
  useEffect(() => {
    const confettiInstance = startConfetti();

    return () => {
      if (confettiInstance) {
        confettiInstance.reset();
      }
    };
  }, []);
};