import { Box, Text, Link } from '@chakra-ui/react';

export const ImprintContent = () => {
  const linkHoverStyle = {
    bgGradient: 'linear(to-l, #7928CA, #FF0080)',
    bgClip: 'text'
  };

  return (
    <Box p={5}>
      <Text as='h1' fontSize='2xl' mb={4}>Impressum</Text>
      <Text as='h2' fontSize='xl' mb={3}>Angaben gemäß § 5 TMG</Text>
      <Text>Golixxo GmbH</Text>
      <Text>Breitlacherstraße 64</Text>
      <Text>60489 Frankfurt am Main</Text>
      <Text mb={3}>Handelsregister: HRB 129360</Text>
      <Text>Registergericht: Amtsgericht Frankfurt am Main</Text>
      <Text mb={3}>Vertreten durch:</Text>
      <Text>Patrick Kraemer</Text>
      <Text as='h2' fontSize='xl' mt={5} mb={3}>Kontakt</Text>
      <Text>Telefon: +49 (0) 176 41752218</Text>
      <Text>E-Mail: <Link href='mailto:hello@softgen.ai' color='teal.500' _hover={linkHoverStyle}>hello@softgen.ai</Link></Text>
      <Text as='h2' fontSize='xl' mt={5} mb={3}>Umsatzsteuer-ID</Text>
      <Text>Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz:</Text>
      <Text mb={3}>DE358401854</Text>
      <Text as='h2' fontSize='xl' mt={5} mb={3}>Redaktionell verantwortlich</Text>
      <Text>Patrick Kraemer</Text>
      <Text>Breitlacherstraße 64,</Text>
      <Text mb={3}>60489 Frankfurt am Main</Text>
      <Text as='h2' fontSize='xl' mt={5} mb={3}>EU-Streitschlichtung</Text>
      <Text>Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit:</Text>
      <Link href='https://ec.europa.eu/consumers/odr/' isExternal color='teal.500' _hover={linkHoverStyle}>https://ec.europa.eu/consumers/odr/</Link>
      <Text>Unsere E-Mail-Adresse finden Sie oben im Impressum.</Text>
      <Text as='h2' fontSize='xl' mt={5} mb={3}>Verbraucher­streit­beilegung/Universaldschlichtungsstelle</Text>
      <Text>Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.</Text>
      <Text mt={5}>Quelle: <Link href='https://www.e-recht24.de' isExternal color='teal.500' _hover={linkHoverStyle}>e-recht24.de</Link></Text>
    </Box>
  );
};
